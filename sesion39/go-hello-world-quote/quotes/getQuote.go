package quotes

import (
	"strings"

	"rsc.io/quote/v3"
)

// GetQuote to show how to export a function
func GetQuote(requiredQuote string) string {
	var result string
	switch strings.ToLower(requiredQuote) {
	case "concurrency":
		result = quote.Concurrency()
	case "glass":
		result = quote.GlassV3()
	case "go":
		result = quote.GoV3()
	case "hello":
		result = quote.HelloV3()
	case "opt":
		result = quote.OptV3()
	}
	return result
}
