import { getProductHTML, templateString } from "../strings";
import { getProductHTMLTestcases, templateStringTestcases } from "./testcases/strings-testcases";

templateStringTestcases.forEach(function(index) {
  const entrada = templateString(index.entrada)
  const salida = index.salida
  expect(entrada).toBe(salida)
});
getProductHTMLTestcases.forEach(testElement =>
  test(`getProductHTML test with: ${testElement.entrada.name}`, () => {
    const entrada = getProductHTML(testElement.entrada)
    const salida = testElement.salida
    expect(entrada).toBe(salida)
  })
)