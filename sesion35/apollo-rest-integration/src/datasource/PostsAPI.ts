import { RESTDataSource } from "apollo-datasource-rest";
import { IPost } from "../types";

export class PostsAPI extends RESTDataSource {

  constructor() {
    super()
    this.baseURL = 'https://jsonplaceholder.typicode.com'
  }

  async createPost(post: IPost): Promise<IPost> {
    return await this.post(`/posts`, post)
  }

  async getPosts(): Promise<Array<IPost>> {
    return await this.get(`/posts`)
  }

  async getPost(id: number): Promise<IPost> {
    return await this.get(`/posts/${id}`)
  }

  async updatePost(id: number, post: IPost): Promise<IPost> {
    return await this.patch(`/posts/${id}`, post)
  }

  async removePost(id: number): Promise<IPost> {
    return await this.delete(`/posts/${id}`)
    // return { success: true, postDeleted: id }
  }

}