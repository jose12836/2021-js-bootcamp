import { NextFunction, Request, Response } from "express"
import { verify } from "jsonwebtoken"

export const authorization = async (req: Request, res: Response, next: NextFunction) => {
  console.log(new Date().toISOString());
  let token = req.headers.authorization
  token = token.split(' ')[1]
  console.log(token);
  if (!token) {
    return res.status(401).send({
      success: false,
      msg: 'missing token'
    })
  } else {
    try {
      const decoded = verify(token, process.env.JWT_SECRET)
      console.log(decoded) // aquí podemos hacer algo con el payload
      next()
    } catch (err) {
      return res.status(401).send({
        success: false,
        msg: err.message
      })
    }
  }

  /**
   * TODO: implementación
   * [done] leer header authorization (aquí debe venir jwt)
   * si jwt existe
   *  [done] verificar que es válido
   *  [done] si es válido, pasar a la siguiente función
   *  [done] si inválido regresar unauthorized
   * si no existe
   *  [done] regresar unauthorized && missing token
   */
}