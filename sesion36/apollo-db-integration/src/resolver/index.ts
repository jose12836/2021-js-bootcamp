export const resolvers = {
  Query: {

    books: (_, __, { dataSources }) => dataSources.booksAPI.getBooks(),

    book: (_, { id }, { dataSources }) => dataSources.booksAPI.getBook(id),

  },

  Mutation: {

    saveBook: (_, { input }, { dataSources }) =>
      dataSources.booksAPI.saveBook({ ...input }),

    updateBook: (_, { id, input }, { dataSources }) =>
      dataSources.booksAPI.updateBook({ id, ...input }),

    deleteBook: (_, { id }, { dataSources }) =>
      dataSources.booksAPI.deleteBook(id),

  }
}