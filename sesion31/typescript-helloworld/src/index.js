var helloWorld = "Hello World";
var user = {
    id: 0,
    // username: "Hayes", // solo nodos que existan
    name: "Hayes",
    // is_admin: true,       // hay que homogenizar el uso de variables
    isAdmin: true
};
var UserAccount = (function () {
    function UserAccount(name, id, isAdmin) {
        this.id = id;
        this.name = name;
        this.isAdmin = isAdmin;
    }
    return UserAccount;
})();
var userAcc = new UserAccount("Murphy", 1, true);
