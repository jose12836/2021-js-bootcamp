package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// SaveUser ...
func SaveUser(c echo.Context) error {
	return c.String(http.StatusOK, "User saved")
}

// GetUser ...
func GetUser(c echo.Context) error {
	id := c.Param("id")
	if id != "" {
		return c.String(http.StatusOK, id+"User properties ")
	}
	return c.String(http.StatusOK, "User List")
}

// UpdateUser ...
func UpdateUser(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "User updated "+id)
}

// DeleteUser ...
func DeleteUser(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "User deleted "+id)
}
