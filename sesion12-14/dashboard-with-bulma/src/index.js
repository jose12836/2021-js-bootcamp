import './sass/styles.scss'
import '@fortawesome/fontawesome-free/js/all'

// Opción 2: importar sass en archivo JS
// import 'bulma/bulma.sass'
// Opción 3: importar css en archivo JS (aquí no hay preprocesamiento)
// import 'bulma/css/bulma.css'

(process.env.NODE_ENV !== 'production')
  ? () => console.log('Development mode 😎✌🏻')
  : () => console.log('Production mode 🚀')

console.log('*** Bulma Dashboard ***')
