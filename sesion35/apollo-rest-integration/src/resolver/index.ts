import { books } from "../db";

export const resolvers = {
  Query: {
    books: () => books,
    book: (_, { id }) => books.find(book => book.id === id),
    posts: async (_, __, { dataSources }) =>
      await dataSources.postsAPI.getPosts(),
    post: async (_, { id }, { dataSources }) =>
      await dataSources.postsAPI.getPost(id),
  },
  Mutation: {
    createPost: async (_, { title, body, userId }, { dataSources }) =>
      await dataSources.postsAPI.createPost({ title, body, userId }),
    updatePost: async (_, { id, title, body, userId }, { dataSources }) =>
      await dataSources.postsAPI.updatePost(id, { title, body, userId }),
    deletePost: async (_, { id }, { dataSources }) =>
      await dataSources.postsAPI.removePost(id)
  }
}