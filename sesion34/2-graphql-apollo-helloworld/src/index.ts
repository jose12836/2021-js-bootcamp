import { ApolloServer, gql } from 'apollo-server'

interface IBook {
  id: number,
  title: string,
  author: string
}

const books: Array<IBook> = [
  {
    id: 1,
    title: "No es lo que dices, sino cómo lo dices",
    author: "Michael Parker"
  },
  {
    id: 2,
    title: "La divina comedia",
    author: "Dante Alighieri"
  },
  {
    id: 3,
    title: "Breve historia del tiempo",
    author: "Stephen Hawking"
  }
]

const typeDefs = gql`
  type Book {
    id: Int
    title: String
    author: String
  }
  type Query {
    books: [Book]
    book(id: Int): Book
  }
`

const resolvers = {
  Query: {
    books: () => books,
    book: (_, { id }) => books.find(book => book.id === id)
  }
}

new ApolloServer({ typeDefs, resolvers })
  .listen()
  .then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
  })
