import { gql } from "apollo-server";

export const Mutation = gql`
  type Mutation {
    createPost( title:String!, body:String!, userId: Int! ): Post
    updatePost( id:Int!, title:String, body:String, userId: Int ): Post
    deletePost(id:Int!): Post
  }
`