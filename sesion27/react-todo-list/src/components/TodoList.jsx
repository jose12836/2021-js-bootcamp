import { TodoItem } from "./TodoItem"

export const TodoList = (props) => {
  const { todos, markComplete, deleteTodo } = props
  return (
    <>
      <h3>TodoList component</h3>
      <ul>
        {todos && todos.map(todo =>
          <li key={todo.id}>
            <TodoItem
              todo={todo}
              markComplete={markComplete}
              deleteTodo={deleteTodo}
            />
          </li>
        )}
      </ul>
    </>
  )
}