export interface IBook {
  id: number,
  title: string,
  author: string
}

export interface IPost {
  id: number
  userId: number
  title: string
  body: string
}