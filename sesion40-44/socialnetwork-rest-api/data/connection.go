package data

import (
	"database/sql"
	"os"
)

// Open connection to DB
func Open() (*sql.DB, error) {
	dn := os.Getenv("SQL_DRIVER_NAME")
	ds := os.Getenv("SQL_CONN_STRING")
	conn, err := sql.Open(dn, ds)
	if err != nil {
		return nil, err
	} else if err := conn.Ping(); err != nil {
		return nil, err
	}
	return conn, nil
}
