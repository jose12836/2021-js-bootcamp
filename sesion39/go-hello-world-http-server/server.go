package main

import (
	"fmt"
	"net/http"
)

func main() {
	port := ":3000"
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Hello World http server!\n")
	})
	http.HandleFunc("/jose", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Hello jose!\n")
	})
	http.ListenAndServe(port, nil)
}
