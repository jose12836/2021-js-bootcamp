import { Connection, getConnection } from "typeorm";
import { UserAPI } from "./db/api/UserAPI";

let connection: Connection
let userAPI: UserAPI

(async () => {
    connection = await getConnection()
    userAPI = new UserAPI(connection)
})()