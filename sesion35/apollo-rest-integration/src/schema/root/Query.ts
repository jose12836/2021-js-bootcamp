import { gql } from "apollo-server";

export const Query = gql`
  type Query {
    books: [Book]
    book(id: Int): Book
    posts: [Post]
    post(id: Int!): Post
  }
`