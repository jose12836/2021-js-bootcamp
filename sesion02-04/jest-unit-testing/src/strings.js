export const templateString = (person) => `Bienvenido ${person.name} ${person.lastname}, entraste como ${person.role}!`

export function getProductHTML(product) {
  let html = `
    <div class="product">
      <div class="product-image">
        <img alt="${product.name}" src="${product.image_url}">
      </div>
      <div class="product-desc">${product.desc}</div>
    </div>`
  return html;
}