import './sass/styles.scss'

(process.env.NODE_ENV !== 'production')
  ? () => console.log('Development mode 😎✌🏻')
  : () => console.log('Production mode 🚀')

console.log('*** Personal page ***')
