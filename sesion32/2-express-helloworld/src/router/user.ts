import express, { NextFunction, Request, Response } from "express";

export const userRouter = express.Router()

// middleware only for this route
userRouter.use((req: Request, res: Response, next: NextFunction) => {
  console.log(req.headers)
  next()
})

userRouter.post('/', (req: Request, res: Response) => {
  res.send(`[${req.method}][${req.baseUrl}][${req.path}]`)
})
userRouter.get('/', (req: Request, res: Response) => {
  res.send(`[${req.method}][${req.baseUrl}][${req.path}]`)
})
userRouter.get('/:userId', (req: Request, res: Response) => {
  res.send(`[${req.method}][${req.baseUrl}][${req.path}][${JSON.stringify(req.params)}]`)
})
userRouter.put('/:userId', (req: Request, res: Response) => {
  res.send(`[${req.method}][${req.baseUrl}][${req.path}][${JSON.stringify(req.params)}]`)
})
userRouter.delete('/:userId', (req: Request, res: Response) => {
  res.send(`[${req.method}][${req.baseUrl}][${req.path}][${JSON.stringify(req.params)}]`)
})