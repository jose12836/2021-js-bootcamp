export const TodoItem = (props) => {
  const { id, title, completed } = props.todo
  const { markComplete, deleteTodo } = props

  const componentStyle = () => {
    return {
      // background: 'deepskyblue',
      margin: '1rem',
      textDecoration: completed ? 'line-through' : 'none',
    }
  }

  return (
    <>
      <input
        type="checkbox"
        checked={completed}
        onChange={e => markComplete(id, e)}
      />
      <label style={componentStyle()}>{title}</label>
      <button onClick={e => deleteTodo(id, e)}>X</button>
    </>
  )
}
