import { ApolloServer } from 'apollo-server'
import { PostsAPI } from './datasource/PostsAPI';
import { resolvers } from "./resolver";
import { typeDefs } from "./schema";

new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => {
    return {
      postsAPI: new PostsAPI()
    }
  }
})
  .listen()
  .then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
  })
