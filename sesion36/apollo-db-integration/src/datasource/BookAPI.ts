import { DataSource } from 'apollo-datasource'
import { Connection, Repository } from 'typeorm'
import { Book } from '../db/entity/Book'
import { IBook } from '../types'

export class BookAPI extends DataSource {

  private repository: Repository<Book>

  constructor(connection: Connection) {
    super()
    this.repository = connection.getRepository(Book)
  }

  async saveBook(book: IBook): Promise<IBook> {
    return await this.repository.save(book)
  }

  async getBooks(): Promise<Array<IBook>> {
    return await this.repository.find()
  }

  async getBook(id: number): Promise<IBook> {
    return await this.repository.findOne(id)
  }

  async updateBook(book: IBook): Promise<IBook> {
    let currentBook: Book = await this.repository.findOne(book.id)
    // eliminamos nodos con valor undefined
    Object.keys(book).forEach(key =>
      book[key] === undefined && delete book[key]
    )
    currentBook = { ...currentBook, ...book }
    return await this.repository.save(currentBook)
  }

  async deleteBook(id: number): Promise<IBook> {
    const currentBook: Book = await this.repository.findOne(id)
    return await this.repository.remove(currentBook)
  }

}
