export const templateStringTestcases = [{
    entrada: {
      name: 'Ethien',
      lastname: 'Salinas',
      role: 'admin'
    },
    salida: "Bienvenido Ethien Salinas, entraste como admin!"
  },
  {
    entrada: {
      name: 'Jazmin',
      lastname: 'Pablo',
      role: 'copywriter'
    },
    salida: "Bienvenido Jazmin Pablo, entraste como copywriter!"
  },
  {
    entrada: {
      name: 'Daniel',
      lastname: 'Domínguez',
      role: 'reviewer'
    },
    salida: "Bienvenido Daniel Domínguez, entraste como reviewer!"
  },
]

export const getProductHTMLTestcases = [
  {
    entrada: {
      name: 'Audifonos',
      image_url: 'https://via.placeholder.com/300X150',
      desc: 'Cupcake candy cake pie icing jelly pastry biscuit. Gummi bears toffee halvah. Oat cake marzipan jujubes tootsie roll muffin caramels cotton candy wafer. Icing marshmallow gummies candy halvah apple pie oat cake.'
    },
    salida: `
    <div class="product">
      <div class="product-image">
        <img alt="Audifonos" src="https://via.placeholder.com/300X150">
      </div>
      <div class="product-desc">Cupcake candy cake pie icing jelly pastry biscuit. Gummi bears toffee halvah. Oat cake marzipan jujubes tootsie roll muffin caramels cotton candy wafer. Icing marshmallow gummies candy halvah apple pie oat cake.</div>
    </div>`
  }
]