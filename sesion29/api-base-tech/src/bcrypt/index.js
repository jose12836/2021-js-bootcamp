const bcrypt = require('bcrypt')
const logger = require('../logger')

const saltRounds = 9;
const myPlaintextPassword = 's0/\/\P4$$w0rD'
const someOtherPlaintextPassword = 'not_bacon'

const hashArr = [
  '$2b$09$jIR4FtjFXp2wfT.aXIsxWODZ4Q1zTheI9x3msK/yzws3pXXjX4qNW',
  '$2b$09$rUxUP4dQBAY30YdKqMGAIePn/GHj75IVteT43v.iL2wwYh43witb2',
  '$2b$09$7x47o/OoGrFtiahM0xKh4OeHucVJ55AuMgw.3upHy6neAULT5Hm1S',
  '$2b$09$k0OHc70rYSWkOmAjmiQIu.kkN85tTPZNwpWtp6Q.wdAbroytbAZzy',
  '$2b$09$3ZVi8pFU88q7KHgn0h7CvuR3jJYJuB4oqKgXNDncfsGyhpuPs/7rW'
]

// Generación de hash para almacenar en BD
bcrypt.hash(myPlaintextPassword, saltRounds)
  .then((hash) => {
    // hash es lo que se almacena en BD
    logger.debug(`myPlaintextPassword : ${hash}`)
  })

// Carga el hash del password desde tu BD.
hashArr.forEach(hash => {
  bcrypt.compare(myPlaintextPassword, hash)
    .then(function (result) {
      logger.debug(`[compare] myPlaintextPassword : ${result}`)
    })

  bcrypt.compare(someOtherPlaintextPassword, hash)
    .then(function (result) {
      logger.debug(`[compare] someOtherPlaintextPassword : ${result}`)
    })
})
