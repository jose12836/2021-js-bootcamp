import { IBook } from "../types";

export const books: Array<IBook> = [
  {
    id: 1,
    title: "No es lo que dices, sino cómo lo dices",
    author: "Michael Parker"
  },
  {
    id: 2,
    title: "La divina comedia",
    author: "Dante Alighieri"
  },
  {
    id: 3,
    title: "Breve historia del tiempo",
    author: "Stephen Hawking"
  }
]