const { format, createLogger } = require('winston');
const winston = require('winston')
require('winston-daily-rotate-file')

const logger = createLogger({
  level: 'debug',
  // format: format.json(),
  format: format.combine(
    format.timestamp(),
    // format.timestamp({format: 'YYYY-MM-DDTHH:mm:ss'}),
    format.errors({ stack: true }),
    format.printf(
      ({ timestamp, level, message }) => `${timestamp} | ${level} | ${message}`
    )
  ),
  defaultMeta: { service: 'api-base-tech' },
  transports: [
    // new winston.transports.File({ filename: 'error.log', level: 'error' }),
    // new winston.transports.File({ filename: 'combined.log' }),
    new winston.transports.DailyRotateFile({
      level: 'error',
      filename: './logs/application-error-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '30m',
      maxFiles: '15d'
    }),
    new winston.transports.DailyRotateFile({
      filename: './logs/application-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '30m',
      maxFiles: '15d'
    }),
  ],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: format.simple(),
  }));
}

module.exports = logger;