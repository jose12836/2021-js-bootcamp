const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: '[name].[contenthash].bundle.js'
  },
  plugins: [
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({ filename: '[name].[contenthash].css' }),
    new HtmlWebpackPlugin({ template: './src/index.html' }),
    new HtmlWebpackPlugin({ template: './src/dashboard.html', filename: 'dashboard.html' }),
    new HtmlWebpackPlugin({ template: './src/books.html', filename: 'books.html' }),
    new HtmlWebpackPlugin({ template: './src/customers.html', filename: 'customers.html' }),
    new HtmlWebpackPlugin({ template: './src/orders.html', filename: 'orders.html' }),

    // ejercicio: cargar dashboard.html

  ],
  module: {
    rules: [
      // // css-loader
      { test: /\.css$/, use: [MiniCssExtractPlugin.loader, 'css-loader'] },
      // // sass-loader
      { test: /\.s[ac]ss$/, use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'] }
    ]
  },
  devServer: {
    inline:true,
    port: 8008
  },
}