require('dotenv').config()

console.log('*** dotenv ***')

// console.log('process.env: ', process.env)
console.log('process.env.NODE_ENV: ', process.env.NODE_ENV)

console.log('process.env.JWT_SECRET: ', process.env.JWT_SECRET)

console.log('process.env.DB_HOST: ', process.env.DB_HOST)
console.log('process.env.DB_USER: ', process.env.DB_USER)
console.log('process.env.DB_PASS: ', process.env.DB_PASS)