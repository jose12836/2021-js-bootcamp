import { gql } from 'apollo-server'
import { Book } from './model/Book'
import { User } from './model/User'
import { Mutation } from './root/Mutation'
import { Query } from './root/Query'

export const typeDefs = gql`
  ${Book}
  ${User}
  ${Query}
  ${Mutation}
`