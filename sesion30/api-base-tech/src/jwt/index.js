require('dotenv').config()
const jwt = require('jsonwebtoken')

// generación de JWT
const token = jwt.sign({
    id: 3,
    name: 'Ethien',
    isAdmin: true
}, process.env.JWT_SECRET, { expiresIn: 60 });
console.log(token);

// verificación de JWT

jwt.verify('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywibmFtZSI6IkV0aGllbiIsImlzQWRtaW4iOnRydWUsImlhdCI6MTYxMTc5NTQzNywiZXhwIjoxNjExNzk1NDk3fQ.H8Zf2tZt6dNdvXG3cXmnGqQJu8PzAUso9Uw0_vch4q0', process.env.JWT_SECRET, (err, decoded) => {

    if (err) {
        console.log('invalid token', err)
    } else {
        console.log('sucess!!! ', decoded);
    }
})