import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Book {

  @PrimaryGeneratedColumn()
  id: number

  @Column()
  title: string

  @Column()
  author: string

  @Column()
  publisher: string

  @Column({ name: 'isbn_10', unique: true, nullable: true })
  isbn10: string

  @Column({ name: 'isbn_13', unique: true, nullable: true })
  isbn13: string

  @Column({ nullable: true })
  category: string

  @Column({ nullable: true })
  year: number

  @Column()
  language: string

  @Column({ name: 'total_pages', nullable: true })
  totalPages: number

}