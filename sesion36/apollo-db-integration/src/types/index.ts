export interface IBook {
  id?: number
  title: string
  author: string
  publisher?: string
  isbn10?: string
  isbn13?: string
  category?: string
  year?: number
  language?: string
  totalPages?: number
}