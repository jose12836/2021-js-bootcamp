export const resolvers = {
  Query: {
    signIn: (_, { email, password }, { dataSources }) => dataSources.authAPI.getToken({ email, password }),

    getAllBooks: (_, __, { dataSources }) => dataSources.booksAPI.getBooks(),
  },

  Mutation: {
    signUp: (_, { input }, { dataSources }) =>
      dataSources.usersAPI.saveUser({ ...input }),

    saveBook: (_, { input }, { dataSources }) =>
      dataSources.booksAPI.saveBook({ ...input }),

    updateBook: (_, { id, input }, { dataSources }) =>
      dataSources.booksAPI.updateBook({ id, ...input }),

    deleteBook: (_, { id }, { dataSources }) =>
      dataSources.booksAPI.deleteBook(id),
  }
}