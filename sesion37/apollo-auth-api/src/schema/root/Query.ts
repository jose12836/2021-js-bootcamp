import { gql } from "apollo-server";

export const Query = gql`
  type Query {
    signIn(email:String!, password:String!): String
    getAllBooks: [Book]
    getBook(id:Int): Book
  }
`