import './scss/styles.scss'
import 'bootstrap/js/src/carousel'
import '@fortawesome/fontawesome-free/js/all'

(process.env.NODE_ENV !== 'production')
  ? () => console.log('Development mode 😎✌🏻')
  : () => console.log('Production mode 🚀')

console.log('*** Bootstrap Website ***')
