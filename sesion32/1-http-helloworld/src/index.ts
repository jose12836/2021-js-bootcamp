import http from 'http';

const hostname: string = '127.0.0.1';
const port: number = 3000;

const server = http.createServer((req, res) => {
  switch (req.method) {
    case 'GET':
      console.log(`req.url: ${req.url}`);
      console.log(`req.method: ${req.method}`);
    
      res.statusCode = 200;
      res.setHeader('Content-Type', 'text/plain');
      res.end('Hello World');
      break;
    case 'POST':
      if(req.url=='/login' || req.url=='/users'){
        console.log(`req.url: ${req.url}`);
        console.log(`req.method: ${req.method}`);
      
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World');
      }
    break;
  
    default:
      if(req.url=='/users'){
        console.log(`req.url: ${req.url}`);
        console.log(`req.method: ${req.method}`);
      
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World');
      }else{
        res.end('');

      }
      break;
  }
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});