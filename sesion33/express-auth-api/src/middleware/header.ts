import { NextFunction, Request, Response } from "express"

export const printHeaders = (req: Request, res: Response, next: NextFunction) => {
    console.log(req.headers)
    next()
}