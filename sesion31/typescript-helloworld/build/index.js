var helloWorld = "Hello World";
var user = {
    id: 0,
    name: "Hayes",
    isAdmin: true
};
var UserAccount = (function () {
    function UserAccount(name, id, isAdmin) {
        this.id = id;
        this.name = name;
        this.isAdmin = isAdmin;
    }
    return UserAccount;
})();
var userAcc = new UserAccount("Murphy", 1, true);
